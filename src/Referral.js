import React, { Component } from 'react';
import {faucets, useTracked} from "./Context";

import {Button} from 'primereact/button';


const referralList = () => {
    const [state, dispatch] = useTracked();
    return faucets.map(el => ({ ref: this.refLink(el, dispatch), state: this.refState(el, state) }));
}

export default class Referral extends Component {

    refLink(faucet, dispatcher) {
        return (
            <a href={faucet.url + '?ref=' + faucet.referralID}
               target='_blank'
               rel="noopener noreferrer"
               onClick={() => (
                   dispatcher({type: 'refUsed', label: faucet.label})
               )}
            >{faucet.label}</a>
        )
    }

    refState(faucet, state) {
        const refStates = state.referral;
        if (refStates[faucet.label]) {
            return (<i className="fa fa-fw fa-check-circle" style={{color: 'green'}} />)
        } else {
            return (<i className="fa fa-fw fa-times" style={{color: 'red'}}/>)
        }
    }

    render () {
        return (
            <div className="content" style={{textAlign:"left", padding:"2em"}}>
                <h1>Get Started</h1>
                <h2>1. CoinPot</h2>
                <p>
                    You need a CoinPot account for instant micro-payments of your earnings.
                </p><p>
                Register or Sign in on <a href="https://coinpot.co/" target="_blank" rel="noopener noreferrer">https://coinpot.co/</a><br/>
                ...but also remember to come back 😉🙊
            </p>
                <h2>2. Moon Faucets</h2>
                <p>
                    Once you've created a CoinPot account you are ready to claim from the Moon Faucets <br/>
                    🌒 ⇒ 💰 💲 💱
                </p>
                <h2>3. Referral Program</h2>
                <p>We don't need any of your credentials but we use cookies to personalize your user experience 🔐 🍪 🙌</p>
                <p>❗❗For Sign-In each Faucet will be opened in a new Tab❗❗</p>
                <Button label="Agree" icon="fa fa-fw fa-check" iconPos="right" onClick={() =>
                    faucets.forEach(f => window.open(f.url + '?ref=' + f.referralID, '_blank'))
                } />
            </div>
        )
    }
}