import React, { Component } from 'react';

import Iframe from 'react-iframe'


export default class CoinPot extends Component {

    constructor() {
        super();
        this.state = {};
    }

    render() {
        const cpURL = "https://coinpot.co/dashboard";

        return (
            <div className="coinpot-content">
                <Iframe url={cpURL} width="100%"  height="100%" id="iframe" className="CoinPotFrame"
                        display="initial"
                        sandbox="allow-forms allow-pointer-lock allow-same-origin allow-scripts"
                        position="relative"/>
            </div>
        )
    }
}