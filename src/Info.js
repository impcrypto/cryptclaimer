import React from 'react';
import {DataTable} from 'primereact/datatable';
import {Column} from 'primereact/column';

import {useTracked} from './Context'
import {faucets} from './Context'



export const Info = () => {
        return (
            <div className="content" style={{textAlign:"left", padding:"2em"}}>
                <h1>CryptoClaimer</h1>
                <h2>What is CryptoClaimer?</h2>
                <p>
                    CryptoClaimer is a simple rotator for Moon Faucets. CryptoClaimer makes your life easier
                    and saves some clicks on each of your claim and thus makes your efforts more convenient and profitable.
                </p>
                <h2>What are Moon Faucets?</h2>
                <p>
                    Moon Faucets are the highest paying crypto faucets out there. You decide how often you claim your earned cryptos.
                    You may prefer to claim a smaller amount every 5 minutes, or visit once per day and claim the large amount that has built up while you were away!
                </p>
                <h2>How are my earnings paid?</h2>
                <p>
                    The Moon Faucets use <a href="https://coinpot.co/" target="_blank" rel="noopener noreferrer">CoinPot</a> for instant payment of your earnings.
                    If you do not have a CoinPot account then you must <a href="https://coinpot.co/register" target="_blank" rel="noopener noreferrer">register first</a>.
                    However, CryptoClaimer is neither particularly associated with nor responsible for CoinPot or the Faucets.
                    CryptoClaimer simply embed the existing infrastructure for the sake of better user experience.
                </p>
            </div>
        )
};