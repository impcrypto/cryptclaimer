import { useReducer } from 'react';

import { createContainer } from 'react-tracked';

const useValue = ({ reducer, initialState }) => useReducer(reducer, initialState);
export const { Provider, useTracked } = createContainer(useValue);

export const faucets = [
    {label: "Dash", url: "http://moondash.co.in/", referralID: "711D5658C563", currencyID: "131", color: "#2775BC"},
    {label: "BCH", url: "http://moonbitcoin.cash/", referralID: "5CE779893CA5", currencyID: "1831", color: "#4CC948"},
    {label: "LTC", url: "https://moonliteco.in/", referralID: "FC6C873B952B", currencyID: "2", color: "#BEBEBE"},
    {label: "Doge", url: "https://moondoge.co.in/", referralID: "8706FA957172", currencyID: "74", color: "#F9C001"},
    {label: "BTC", url: "https://moonbit.co.in/", referralID: "DE948548159C", currencyID: "1", color: "#F79301"},
];

export const initialState = {
    menuVisible: false,
    faucetIdx: 0,
    referral: {
    }
};

export const currentIdx = (cur) => {
    return cur % faucets.length
};

export const nextIdx = (cur) => {
    return (currentIdx(cur) + 1) % faucets.length
};

export const nextFaucet = (cur) => {
    return faucets[nextIdx(cur)]
};

export const prevIdx = (cur) => {
    return (currentIdx(cur) <= 0) ? faucets.length-1 : currentIdx(cur)-1
};

export const prevFaucet = (cur) => {
    return faucets[prevIdx(cur)]
};

export const reducer = (state, action) => {
    switch (action.type) {
        case 'flipNavigation': return { ...state, menuVisible: !state.menuVisible};
        case 'increment': return { ...state, faucetIdx: nextIdx(state.faucetIdx) };
        case 'decrement': return { ...state, faucetIdx: prevIdx(state.faucetIdx) };
        case 'restart': return { ...state, faucetIdx: 0 };
        case 'refUsed': return { ...state, referral: { ...state.referral, [action.label]: true }};
        default: throw new Error(`unknown action type: ${action.type}`);
    }
};